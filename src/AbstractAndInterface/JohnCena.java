package AbstractAndInterface;

public class JohnCena extends Wrestler{

    @Override
    public void wrestlerName() {
        System.out.println("John Cena");
    }

    @Override
    public void themeMusic() {
        System.out.println("You Can't See Me");
    }

    @Override
    public void finisher() {
        System.out.println("Attitude Adjustment");
    }

}
