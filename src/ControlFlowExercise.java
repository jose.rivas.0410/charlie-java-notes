import java.util.Scanner;

public class ControlFlowExercise {
    public static void main(String[] args) {

//        Regular code
//        int i = 9;
//        while (i <= 23) {
//            System.out.print(i + " ");
//            i++;
//        };
//        For Loop Code
//        for (var i = 9; i <= 23; i++) {
//            System.out.println(i + " ");
//        }

//        Regular code
//        int i = 0;
//        do {
//            System.out.print(i + " ");
//            i+=2;
//        } while (i <= 100);
//        For Loop Code
//        for (var i = 2; i <= 100; i+=2) {
//            System.out.println(i + " ");
//        }

//        Regular code
//        int i = 100;
//        do {
//            System.out.print(i + " ");
//            i-=5;
//        } while (i >= -10);
//        For Loop Code
//        for (var i = 100; i >= -10; i-=5) {
//            System.out.println(i + " ");
//        }

//        Regular code
//        long i = 2;
//        do {
//            System.out.println(i);
//            i*=i;
//        } while (i < 1000000);
//        For Loop Code
//        for (long i = 2; i < 1000000; i*=i) {
//            System.out.println(i + " ");
//        }

//        for (var i = 1; i <= 100; i++) {
//            if (i % 3 == 0 && i % 5 == 0) {
//                System.out.println("FIZZBUZZ");
//            }
//            else if (i % 3 == 0) {
//                System.out.println("FIZZ");
//            }
//            else if (i % 5 == 0) {
//                System.out.println("BUZZ");
//            }
//            else {
//                System.out.println(i);
//            }
//        }

        Scanner sc = new Scanner(System.in);
//        String choice = "y";
//
//        while(choice.equalsIgnoreCase("y")) {
//
//            System.out.println("What number would you like to go up to?: ");
//            int integerNext = sc.nextInt();
//            System.out.println("Here is your table");
//
//            System.out.println("Number" + " | " + "Squared" + " | " + "Cubed");
//            System.out.println("======" + " | " + "=======" + " | " + "=====");
//
//            for(int i = 1; i <= integerNext; i++) {
//
//                int numberSquared = (int) Math.pow(i, 2);
//                int numberCubed = (int) Math.pow (i, 3);
//                System.out.format("%-6d | %-6d  | %-6d%n", i, numberSquared, numberCubed);
//            }
//                System.out.print("Continue? (y/n): ");
//                choice = sc.next();
//                System.out.println();
//        }

//        System.out.println("Enter your grade: ");
//        String choice = "y";
//        int userInput = sc.nextInt();
//
//        while (choice.equalsIgnoreCase("y")) {
//            switch (userInput) {
//                case 100-90:
//                    System.out.println("A: 100 - 90");
//                    break;
//                case 89-80:
//                    System.out.println("B: 89 - 80");
//                    break;
//                case 79-70:
//                    System.out.println("C: 79 - 70");
//                    break;
//                case 69-60:
//                    System.out.println("D: 69 - 60");
//                    break;
//                case 59-0:
//                    System.out.println("F: 59 - 0");
//                    break;
//                default:
//                    System.out.println("Invalid entry");
//
//            }
//            System.out.print("Continue? (y/n): ");
//                choice = sc.next();
//                System.out.println();
//        }

//        Stephen's Code
//        String input;
//        do {
//            System.out.println("Enter a grade number: ");
//            int grade = sc.nextInt();
//            if (grade >= 90 && grade <= 100) {
//                System.out.println("A");
//            }
//            else if (grade >= 80 && grade <= 89) {
//                System.out.println("B");
//            }
//            else if (grade >= 70 && grade <= 79) {
//                System.out.println("C");
//            }
//            else if (grade >= 60 && grade <= 69) {
//                System.out.println("D");
//            }
//            else if (grade >= 0 && grade <= 59) {
//                System.out.println("F");
//            }
//            else {
//                System.out.println(grade);
//            }
//            System.out.println("Would you like to continue?");
//            input = sc.next();
//        } while (input.equalsIgnoreCase("yes"));
//        System.out.println("Have a nice day");



    }
}
