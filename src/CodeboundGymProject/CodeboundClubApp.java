package CodeboundGymProject;

import java.util.LinkedList;

public class CodeboundClubApp {
    public static void main(String[] args) {
        String mem;
        MembershipManagement mm = new MembershipManagement();
        MemberFileHandler mh = new MemberFileHandler();
        LinkedList<Member> members = mh.readFile();
        while (true) {
            int choice = mm.getChoice();
            if (choice == 1) {
//                mm.addMembers(members);
                mh.appendFile(mm.addMembers(members));
            } else if (choice == 2) {
//                mm.removeMember(members);
                mh.overwriteFile(members);
            } else if (choice == 3) {
                mm.printMemberInfo(members);
            }else if (choice == -1) {
                return;
            } else {
                System.out.println("Sorry, invalid entry");
            }
        }
    }
}
