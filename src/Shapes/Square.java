package Shapes;

public class Square extends Quadrilateral{
    public Square(int side) {
        super(side, side);
    }

    @Override
    public void setLength() {

    }

    @Override
    public void setWidth() {

    }

    @Override
    public double getPerimeter() {
        return 0;
    }

    @Override
    public double getArea() {
        return 0;
    }
}
