package Inheritance;
// how do you INHERIT from another class?
// use the 'extends keyword

public class Dog extends Animal {
    // FIELDS for our Dog class
    private int eyes;
    private int legs;
    private int tail;
    private int teeth;
    private String fur;


    // WE NEED TO PASS IN THE FIELDS IN THE CONSTRUCTOR

    // CONSTRUCTOR
    public Dog(String name, int brain, int body, int size, int weight, int eyes, int legs, int tail, int teeth, String fur) {
        super(name, brain, body, size, weight); // this line must be first in the child's constructor
        // ^ this is initializing the BASE / PARENT / SUPER characteristics of an animal

        this.eyes = eyes;
        this.legs = legs;
        this.tail = tail;
        this.teeth = teeth;
        this.fur = fur; // initialized all the fields related to Dog (Child / Subclass)
    }


    // METHODS
    public void chew() {
        System.out.println("This dog is chewing his food...");
    }

    // unique to the dog class


    // OVERRIDING METHODS - inherit a method but make it unique for our class
    @Override
    public void eat() {
        System.out.println("This dog is eating...");
//        this.chew();
        chew();

        // calling the Animal's eat() inside of Dog's eat()
        super.eat();

        // super keyword - allows us to access a superclasses methods and constructors from within a subclass
    }
}
