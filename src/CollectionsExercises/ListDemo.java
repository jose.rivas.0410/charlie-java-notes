package CollectionsExercises;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {
    public static void main(String[] args) {
        List<Integer> userAgeList = new ArrayList<>();
        userAgeList.add(40);
        userAgeList.add(53);
        userAgeList.add(45);
        userAgeList.add(53);

        System.out.println(userAgeList); // [40, 53, 45, 53]

        userAgeList.add(2, 53);
        System.out.println(userAgeList); // [40, 53, 53, 45, 53] *added new second index*

        userAgeList.set(3, 49);
        System.out.println(userAgeList); // [40, 53, 53, 49, 53] *changed third index to new int*

        userAgeList.remove(3);
        System.out.println(userAgeList); // [40, 53, 53, 53] *removed third index*

        userAgeList.get(2);
        System.out.println(userAgeList); // [40, 53, 53, 53] *ignored get()*

        System.out.println(userAgeList.size()); // 4

        System.out.println(userAgeList.contains(12)); // false
    }
}
