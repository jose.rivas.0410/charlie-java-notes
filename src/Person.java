public class Person {
    private String name;
    public Person(){}

    public Person(String name){
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
//- returns the person's name
    public void setName(String name) {
        this.name = name;
    }
//- changes the name property to the passed value
    public String sayHello() {
        return String.format("Hello, my name is %s", name);
    }
//- prints a message to the console using the person's name

    public static void main(String[] args) {
        Person p1 = new Person("Jimmy");
        System.out.println(p1.getName());
        p1.setName("Corn");
        System.out.println(p1.name);
        System.out.println(p1.sayHello());


//        Person person1 = new Person("John");
//        Person person2 = new Person("John");
//        System.out.println(person1.getName().equals(person2.getName())); // true
//        System.out.println(person1 == person2); // false


//        Person person1 = new Person("John");
//        Person person2 = person1;
//        System.out.println(person1 == person2); // true


        Person person1 = new Person("John");
        Person person2 = person1;
        System.out.println(person1.getName()); // John
        System.out.println(person2.getName()); // John
        person2.setName("Jane");
        System.out.println(person1.getName()); // Jane
        System.out.println(person2.getName()); // Jane

    }
}
