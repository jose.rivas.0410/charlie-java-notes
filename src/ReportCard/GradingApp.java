package ReportCard;

import java.util.HashMap;
import java.util.Scanner;

public class GradingApp {
    public static void main(String[] args) {
        HashMap<String, Student> students = new HashMap<>();
        Student s1 = new Student("Peter");
            s1.addGrade(89);
        Student s2 = new Student("John");
            s2.addGrade(76);
        Student s3 = new Student("Alexa");
            s3.addGrade(94);
        Student s4 = new Student("Tiffany");
            s4.addGrade(98);
        students.put("Kaiser23", s1);
        students.put("DragonGod34", s2);
        students.put("LadybugLover21", s3);
        students.put("StBkLvrXOXO66", s4);

//        System.out.println(students.get(s1.getName()));

        String choice = "n";
        Scanner sc = new Scanner(System.in);
        do {
        System.out.println("What student would you like to see? ");
        String userInput = sc.nextLine();
            if (userInput.equalsIgnoreCase("Kaiser23")) {
                System.out.println(s1.getName());
                System.out.println("Grade: " + s1.getGradeAverage());
            } else if (userInput.equalsIgnoreCase("DragonGod34")) {
                System.out.println(s2.getName());
                System.out.println("Grade: " + s2.getGradeAverage());
            } else if (userInput.equalsIgnoreCase("LadybugLover21")) {
                System.out.println(s3.getName());
                System.out.println("Grade: " + s3.getGradeAverage());
            } else if (userInput.equalsIgnoreCase("StBkLvrXOXO66")) {
                System.out.println(s4.getName());
                System.out.println("Grade: " + s4.getGradeAverage());
            } else if (!userInput.equalsIgnoreCase("Kaiser23") && !userInput.equalsIgnoreCase("DragonGod34") && !userInput.equalsIgnoreCase("LadybugLover21") && !userInput.equalsIgnoreCase("StBkLvrXOXO66")) {
                System.out.println("You have entered an incorrect username");
            } else {
                System.out.println("Have a nice day!");
            }
            System.out.println("Continue: [y/n]");
            choice = sc.next();
            System.out.println();
            sc.nextLine();
        } while (choice.equalsIgnoreCase("y"));


    }
}
