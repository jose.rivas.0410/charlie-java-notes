import java.util.Arrays;

public class ArraysExercise {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(numbers)); // will print the array into a string

        // if 6 is true
        System.out.println(firstLast6(numbers));

        // sum of array
        System.out.println("Sum of array is: " + sum(numbers));

        // reverse array
        reverse(numbers, numbers.length);

        // person array
        System.out.println();
        int n = 3;
        String i;
        String[] person = { "John", "Phil", "Ryoto" };
        System.out.println("Initial Array:\n" + Arrays.toString(person));
        String x = "Joshua";
        person = addPerson(n, person, x);
        System.out.println("Array with " + x + " added:\n" + Arrays.toString(person));
}

    public static boolean firstLast6(int[] numbers) {
        return (numbers[0] == 6 || numbers[numbers.length - 1] == 6);
    }

    public static void reverse(int[] numbers, int n) {
        int[] dest = new int[n];
        int j = n;
        for (int i = 0; i < n; i++) {
            dest[j - 1] = numbers[i];
            j = j - 1;
        }
        System.out.println("Reversed array: ");
        for (int k = 0; k < n; k++) {
            System.out.print(dest[k] + " ");
        }
    }

    public static int sum(int[] numbers) {
        int sum = 0;
        int i;
        for (i = 0; i < numbers.length; i++)
            sum +=  numbers[i];
        return sum;
    }

    public static String[] addPerson(int n, String[] person, String x) {
        int i;
        String[] newarr = new String[Integer.parseInt(String.valueOf(n + 1))];
        for (i = 0; i < n; i++)
            newarr[i] = person[i];
        newarr[Integer.parseInt(String.valueOf(n))] = x;
        return newarr;
    }
}
