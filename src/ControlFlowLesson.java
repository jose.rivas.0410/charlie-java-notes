import java.util.Scanner;

public class ControlFlowLesson {
    public static void main(String[] args) {
//        Comparison Operators
//        ==, >, <, <=, >=, !=

//        System.out.println(5 != 2); // true
//        System.out.println(5 >= 5); // true
//        System.out.println(5 <= 5); // true

//        Logical Operators
//        &&, ||
//        System.out.println(5 == 6 && 2>1 && 3 !=3); // false
//        System.out.println(5 != 6 && 2>1 && 3 !=3); // false
//        System.out.println(5 == 6 && 2>1 && 3 ==3); // true

//        .equals(), .equalsCase() - replaces the '==='

//        if statements
//        int score = 34;
//        if (score >= 30) {
//            System.out.println("New high score");
//        } else {
//            System.out.println("Try again...");
//        }

//        Switch Statements
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Enter your grade: ");
//        String userInput = scanner.nextLine().toUpperCase();
//
//        userInput = userInput.toUpperCase();
//        switch (userInput) {
//            case "A":
//                System.out.println("Distinction");
//                break;
//            case "B":
//                System.out.println("Grade: B");
//                break;
//            case "C":
//                System.out.println("Grade: C");
//                break;
//            case "D":
//                System.out.println("Grade: D");
//                break;
//            case "F":
//                System.out.println("Fail");
//                break;
//            default:
//                System.out.println("Invalid entry");
//
//        }

//        While Loop
//        int i = 0;
//        while (i <= 10) {
//            System.out.println("i is " + i);
//            i++;
//        }


//        Do while Loop
//        do {
//            System.out.println("You will see this!");
//        } while (false);

//        For Loop
//        for (var i = 1; i <= 10; i++) {
//            System.out.println(i);
//        }

    }
}
