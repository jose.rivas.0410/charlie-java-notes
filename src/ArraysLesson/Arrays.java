package ArraysLesson;

public class Arrays {
    /*
    In Java arrays are a different kind of object that contains zero or more
    items called ELEMENTS
    Array elements can be any valid type but all alements MUST be of
    the same type

    Syntax:
    type[] nameOfTheVariable;
     */

    double[] prices;

    /*
    In Java arrays have a fixed length, this is accessed by using the .length property
    They have to be defined when they are created

    The size of the array can either by a literal, a constant, or a variable
    arrays are zero index
     */

    public static void main(String[] args) {
//        Example:
        String[] developers = new String[5];
        developers[0] = "Josh";
        developers[1] = "Nick";
        developers[2] = "Jose";
        developers[3] = "Stephen";
        developers[4] = "Karen";

//        System.out.println(developers[3]); // Stephen
//        System.out.println(developers.length); // 5


        // Assigning a variable we created a new array where the size is
        // determined by a constant defined before

//        int numOfBugs = (int) Math.floor(Math.random() * 100);
//        Bug[] myCode = new Bug[numOfBugs];

        int[] numbers = new int[3];
        numbers[0] = 11;
        numbers[1] = 12;
        numbers[2] = 13;


        // ArrayIndexOutOfBoundsException
//        numbers[3] = 14;
//        System.out.println(numbers[3]);

        // iterating
        // like js, we can use the length property of an array in combo with
        // looping construct object(s) to iterate it

        String[] lang = {"html", "css", "javascript", "jquery", "angular", "java"};
        for (int i = 0; i < lang.length; i++) {
//            System.out.println(lang[i]);
        }


        // alter / "Enhanced for" (Java's forEach)
        String[] gameConsoles = {"ps4", "xbox1", "nintendo switch", "sega"};
        for (String gc : gameConsoles) {
//            System.out.println(gc);
        }

        // two-dimentional array
        // "matrix" / grid-like array

        int[][] myMatrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        // access the first element inside of the second row
//        System.out.println(myMatrix[1][0]); //4

        // last element first row
//        System.out.println(myMatrix[0][2]); // 3

        // last element last row
//        System.out.println(myMatrix[2][2]); // 9

        // iterate through the Matrix using a nested loop

        for (int[] row : myMatrix) { // first loop targeting the first array (row)
            System.out.println("+---+---+---+");
            System.out.print("| ");

            for (int n : row) { // second loop targeting the second array (individual number
                System.out.print(n + " | ");
            }
            System.out.println();
        }
        System.out.println("+---+---+---+");
    }
}
