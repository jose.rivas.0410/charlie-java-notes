import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CollectionsArraysExercise {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>(Arrays.asList(
                1,
                2,
                3,
                4
        ));
        ArrayList<String> cohort = new ArrayList<>(Arrays.asList(
                "Josh",
                "Nick",
                "Jose",
                "Stephen"
        ));

        cohort.add("Karen");
        System.out.println(integers);
        System.out.println(cohort);

        integers.remove(2);
        System.out.println(integers);

        ArrayList<String> dogBreeds = new ArrayList<>();
        dogBreeds.add("Rottweiler");
        dogBreeds.add("German Shepherd");
        dogBreeds.add("Chihuahua");
        dogBreeds.add("Pug");
        dogBreeds.add("Pitbull");
        dogBreeds.add("English Bulldog");
        Collections.sort(dogBreeds);
        System.out.println(dogBreeds);


        ArrayList<String> catBreeds = new ArrayList<>();
        catBreeds.add("Rottweiler");
        catBreeds.add("German Shepherd");
        catBreeds.add("Chihuahua");
        catBreeds.add("Pug");
        catBreeds.add("Pitbull");
        catBreeds.add("English Bulldog");
        System.out.println(catBreeds.lastIndexOf("Pug"));
        Collections.reverse(catBreeds);
        System.out.println(catBreeds);

    }
}
