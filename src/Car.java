public class Car {
    private String name;
    String maker;
    String model;
    int year;

    public Car(String name, String maker, String model, int year) {
        this.name =name;
        this.maker = maker;
        this.model = model;
        this.year = year;
    }

    public String getCarInfo() {
        return String.format("Car Name: %s\nMake: %s\nModel: %s\nYear: %s", name, maker, model, year);
    }

    public static void main(String[] args) {
        Car cr1 = new Car("Betty", "Nissan", "Altima", 2019);
        Car cr2 = new Car("Bill", "Toyota", "Prius", 2012);
        Car cr3 = new Car("Nancy", "Alfa Romeo", "Giulia", 2020);
        System.out.println(cr1.getCarInfo());
        System.out.println(cr2.getCarInfo());
        System.out.println(cr3.getCarInfo());
    }
}
