package Validation;

import java.util.Scanner;

public class Input {
    private Scanner sc;

    public Input(Scanner sc) {
        this.sc = sc;
    }

    public String getString() {
        return this.sc.nextLine();
    }

    public boolean yesNo() {
        String choice = "y";
        while (choice.equalsIgnoreCase("y")) {
            System.out.print("Continue? (y/n): ");
            choice = sc.next();
            System.out.println();
        }
        return this.sc.nextBoolean();
    }

//    public boolean continueYesNo() {
//        return this.sc.nextBoolean();
//    }

    public int getInt(int min, int max) {
        return this.sc.nextInt();
    }

    public int getInt() {
        return this.sc.nextInt();
    }

    public double getDouble(double min, double max) {
        return this.sc.nextDouble();
    }

    public double getDouble() {
        return this.sc.nextDouble();
    }

}
