package AbstractAndInterface;
/*
INTERFACE - much like abstract class in that they CANNOT BE INSTANTIATED
- instead, they must be implemented by classes or extended by other interfaces
- *** contains only abstract methods ***
An interface is a special case of an abstract class
 */

public interface WWE {
    // What if a wrestler has a bigger pay?

    // abstract methods
    abstract public void wrestlerName();

    public abstract void themeMusic();

    public abstract void finisher();

    public abstract void paymentForPerformance(int hours);
}
