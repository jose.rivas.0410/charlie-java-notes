package Collections;
/*
COLLECTIONS
- a data structure that can be used to group, or collect, objects
The Java standard library, a collection of code that comes with the Java language,
contains many collections to help with common programming tasks

Main Focus:
ArrayList
HashMaps

ArayList
- represents an array that can change it's size





.size() => returns the number of elements in the array
.add() => add an element to the collection (optionally) at a specified index
.get() => return the element at the specified index
.indexOg() => return the first found index of the given item, or -1 if not found
.contain() => checks if a collection contains a given element
.lastIndexOf() => find the last index of the given element. If not found, will return -1
.isEmpty() =>






























 */



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
//        ArrayList

        ArrayList<Integer> numbers = new ArrayList<>(); // created an array of integers named numbers

        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(25);











        ArrayList<String> names = new ArrayList<>(Arrays.asList(
                "Pop",
                "Duncan",
                "Parker",
                "Ginobili"
        ));

        names.add("Robinson");
        System.out.println(names);

        ArrayList<Integer> myNumbers = new ArrayList<>();
        myNumbers.add(23);
        myNumbers.add(50);
        myNumbers.add(32);
        myNumbers.add(91);

        // peeking into the list
        int num = myNumbers.get(0); // 23
        int myNum = myNumbers.get(2); // 32
        System.out.println(num);
        System.out.println(myNum);


        // edit element(s)
//        myNumbers.set(3, 100);
//        System.out.println(myNumbers); // 91 => 100

        // remove elements
        myNumbers.remove(2); // remove 32 from myNumbers array
        System.out.println(myNumbers);

        // reordering list
//        Collections.sort(myNumbers);
//        Collections.reverse(myNumbers);
//        System.out.println(myNumbers);

        ArrayList<String> roasts = new ArrayList<>(Arrays.asList(
                "light",
                "medium",
                "bold",
                "dark"
        ));


        System.out.println(roasts.contains("espresso")); // false
        System.out.println(roasts.lastIndexOf("bold")); // 2
        System.out.println(roasts.isEmpty()); // false
        System.out.println(roasts.indexOf("medium")); // 1

// HashMap
        // syntax:
//        HashMap<Type, Type> nameOfVariable = new HashMap<>();

//        Example
        HashMap<String, String> usernames = new HashMap<>();
        usernames.put("Karen", "krivas123");
        usernames.put("Stephen", "squedea001");
        usernames.put("Juan", "thejuanandonly210");
        usernames.put("Josh", "sleepy512");

        // getting values from and info about hash map
        System.out.println(usernames);

        System.out.println(usernames.get("Juan")); // thejuanandonly210

        System.out.println(usernames.get("Stephen")); // squedea001

        // updating our hash map
        usernames.put("Jose", "jose321");
        System.out.println(usernames);

        usernames.put("Jose", "jose213");
        System.out.println(usernames);

        // replacing in our hash map
        usernames.replace("Stephen", "goat247");
        System.out.println(usernames);


        // remove pairs from hash map
        System.out.println(usernames.remove("Karen")); // krivas123

        System.out.println(usernames); // updates hash map that does not contain krivas123

        usernames.clear();
        System.out.println(usernames); // {}

        System.out.println(usernames.isEmpty()); // true

    }
}
