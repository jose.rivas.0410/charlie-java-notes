import java.util.Scanner;

public class Console {
    public static void main(String[] args) {
        // PRINTING OUT OUR DATA INB THE CONSOLE / TERMINAL

//        String cohort = "Charlie";
//        System.out.println(cohort);
          // Prints out a formatted string
//        System.out.format("Hello there, %s. Nice to see you.\n", cohort);
//
//        String school = "CodeBound";
//
//        System.out.format("Hello there, %s. Welcome to %s", cohort, school);

        // SCANNER CLASS - get input from the console.
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Enter something: "); // prompt the user to enter data
//        String input = scanner.nextLine(); // obtaining the value that the user inputted
//        System.out.println("Thank you, you entered : \n" + input); // "scouting" what the user entered


        Scanner sc = new Scanner(System.in);
//        String userInput = sc.next();
//        String userInput2 = sc.next();
//        System.out.println(userInput);
//        System.out.println(userInput2);
//        .next() captures each input separated by a string
//
//        System.out.println("Please enter your First, Middle, and Last name: ");
//        String first = sc.next();
//        String middle = sc.next();
//        String last = sc.next();
//
//        System.out.println("My full name is " + first + " " + middle + " " + last);


//        .nxtInt() captures the first valid int value
        System.out.println("How old are you? ");
        int age = Integer.parseInt(sc.next()); // nextInt() could also do fine
        System.out.println(age);

    }

}
