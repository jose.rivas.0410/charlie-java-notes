package Shapes;

public class Rectangle extends Quadrilateral implements Measurable {
    protected int length;
    protected int width;

    public Rectangle(int length, int width) {
        super(length, width);
        this.length = length;
        this.width = width;
    }

    @Override
    public void setLength() {

    }

    @Override
    public void setWidth() {

    }

    public double getArea() {
        return length * width;
    }

    public double getPerimeter() {
        return 2 * length + 2 * width;
    }
}
