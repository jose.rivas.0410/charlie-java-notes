package Exceptions;

import java.util.Scanner;

public class GeneralError {
    public static void main(String[] args) {
        int denominator;
        int numerator;
        Scanner userInput = new Scanner(System.in);

        try {
            System.out.println("Enter the numerator");
            numerator = userInput.nextInt();
            System.out.println("Enter the denominator");
            denominator = userInput.nextInt();
            System.out.println("Result is: " + numerator/denominator);
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }finally {
            System.out.println("- - - End of Error Handling Example - - -");
        }
    }
}
