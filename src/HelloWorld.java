public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.out.println("Welcome to Java!");

//        single line comment

        /*
        multi
        line
        comment
        here
         */

//        data types
        /*
        byte - very short integers from the range -128 to 127
        short - short integers from -32,768 to 32,767
        int - integers

        refer to slack's example
         */

//        STRINGS
//        "String / long sentences";
//        'C';

        System.out.println("Escape characters:");
        System.out.println("First \\"); // \
        System.out.println("Second \n"); // new line
        System.out.println("Third \t"); // tab characters

//        VARIABLES
        // all variables in Java MUST be declared before they are used
        // Syntax:
        // dataType nameOfVariable;

        byte age = 12;
        short myShort = -32768;
//        int myInteger;

//        myInteger = 18;
//        System.out.println(myInteger);

        boolean userLoggedIn = false;
        System.out.println(userLoggedIn);

        int num1;
        int num2;
        num1 = 12;
        num2 = 13;
        int sum = num1 + num2;
        System.out.println(sum);
//        System.out.println(12 + 13);

//        CASTING
        /*
        Turning a value of one type into another.
        Two types of casting: implicit / explicit casting

        Implicit Casting
        - involves assigning a value of a less precise data type to a
        variable whose types is of a higher precision
         */
        int myInteger = 900;
        long morePrecise = myInteger;
        System.out.println(morePrecise);

        // Explicit Casting
        double pi = 3.14159;
        int almostPi = (int) pi;
        System.out.println(almostPi);

    }
}
