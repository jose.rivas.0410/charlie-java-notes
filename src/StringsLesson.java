public class StringsLesson {
    public static void main(String[] args) {
//        Strings
//        String message = "Hello World!";
//        System.out.println(message);

//        String anotherMessage
//        String anotherMessage = "Another message assigned!";

//        Concatenation
//        System.out.println(message + " " + anotherMessage);

//        Format
//        System.out.printf("%s %s", message, anotherMessage);

//        String Comparison Methods
//        .equals(), .equalsIgnoreCase(), .startsWith(), .endsWith()

//        String hero = "Batman";

//        if (hero == "Batman") { // DO NOT WANT TO DO THIS!... EVER
//            System.out.println("Nan Nan NANANANNANANANANAN");
//        }

//        DO THIS INSTEAD!
//        if (hero.equals("Batman")) {
//            System.out.println("NANANANANANANA");
//        }
//
//        String input = "Charlie Rocks!";
//        System.out.println(input.equals("charlie rocks!")); // false
//        System.out.println(input.equals("Charlie rocks!")); // false
//        System.out.println(input.equals("CHARLIE ROCKS!")); // false
//        System.out.println(input.equals("Charlie Rocks")); // false

//        System.out.println(input.equalsIgnoreCase("charlie rocks!"));
//        System.out.println(input.equalsIgnoreCase("charlie rocks!"));
//        System.out.println(input.equalsIgnoreCase("CHARLIE ROCKS!"));
//        System.out.println(input.equalsIgnoreCase("CHARLIE ROCKS!"));

//        System.out.println(input.startsWith("c")); // false
//        System.out.println(input.startsWith("C")); // true
//        System.out.println(input.endsWith("s")); // false
//        System.out.println(input.endsWith("!"));// true

//        String manipulation methods
//        char charAt(int index);
//        charAt() - returns the character at the specified index of the string

//        indexOf() - returns the index of the first occurrence of a certain SUBSTRING
        // *returns -1 if the substring is not found

//        lastIndexOf() - like indexOf(), but start the search from the end of the string

        // length() - returns the length of the string

        // replace() - returns a copy of the string that has oldChar replaced with
        // newChar

        // substring() - returns a new substring that starts at a specified index and
        // (optionally) ends at the specified index

        String name = "Charlie";
        System.out.println(name.substring(1)); // harlie

        // toLowerCase() / toUpperCase()

        // trim() - returns a copy of a string w/o leading and trailing whitespace
    }
}
