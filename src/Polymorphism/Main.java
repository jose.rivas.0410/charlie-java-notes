package Polymorphism;

/*
Polymorphism is a feature of Inheritance that allows us to treat object(s) of different
subclasses / child classes tha have the SAME superclass / parent class
as if they were of the superclass type

Methods or variables that are defined with a superclass type
can accept objects that are a subclass of that type

FINAL keyword
- used to prevent inheritance and / or overriding
 */
public class Main {
    public static void main(String[] args) {
        // create a new developer object
        Developer developer1 = new Developer();
        Developer developer2 = new Developer();
        Developer developer3 = new Developer();
        Developer developer4 = new Developer();



//        doingWork(developer1);
//        doingWork(developer2);
//        doingWork(developer3);
//        doingWork(developer4);


//        Polymorphism
        Developer[] codebound = new Developer[5];
        codebound[0] = new Developer();
        codebound[1] = new ScrumMaster();
        codebound[2] = new ScrumMaster();
        codebound[3] = new Developer();
        codebound[4] = new Developer();

        for (Developer d : codebound) {
            doingWork(d);
        }
    }


    // doingWork()
    public static void doingWork(Developer dev) {
        System.out.println(dev.work());
    }
}
