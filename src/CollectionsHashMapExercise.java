import java.util.HashMap;

public class CollectionsHashMapExercise {
    public static void main(String[] args) {
        HashMap<Integer, String> vacationSpots = new HashMap<>();
        vacationSpots.put(1, "Japan");
        vacationSpots.put(2, "Colorado");
        vacationSpots.put(3, "Machu Picchu");
        vacationSpots.put(4, "Italy");
        vacationSpots.put(5, "Paris");
        vacationSpots.put(6, "Tibet");

        System.out.println(vacationSpots);

        System.out.println(vacationSpots.isEmpty()); // false

        System.out.println(vacationSpots.get(2)); // Colorado

        System.out.println(vacationSpots.containsKey(5)); // true

        System.out.println(vacationSpots.containsValue("Japan")); // true

        System.out.println(vacationSpots.keySet()); // [1, 2, 3, 4, 5, 6]

        System.out.println(vacationSpots.values()); // [Japan, Colorado, Machu Picchu, Italy, Paris, Tibet]
    }
}
