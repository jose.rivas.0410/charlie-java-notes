import java.util.Scanner;

public class ConsoleExercise {
    public static void main(String[] args) {

//        double pi = 3.14159;
//        System.out.format("The value of pi is approximately %.2f %n", pi);

        Scanner sc = new Scanner(System.in);
//        System.out.println("How tall are you without inches? ");
//        int height = Integer.parseInt(String.valueOf(sc.nextInt()));
//        System.out.format("You are %sft tall", height);

//        System.out.println("Enter your 3 favorite colors ");
//        String color = sc.next();
//        String color2 = sc.next();
//        String color3 = sc.next();
//        System.out.format("Your three favorite colors are:\n %s\n %s\n %s\n", color, color2, color3);

//        System.out.println("Enter your favorite quote ");
//        String quote = sc.nextLine();
//        System.out.format("You entered:\n %s", quote);

//        System.out.println("Enter your favorite quote ");
//        String quote = sc.nextLine();
//        System.out.println(quote);

        System.out.println("What is your best guess of the perimeter of CodeBound's classroom? Enter the numeric values of the length, width, and height ");
        String length = sc.next();
        float L = Float.parseFloat(length);
        String width = sc.next();
        float W = Float.parseFloat(width);
        String height = sc.next();
        float H = Float.parseFloat(height);
        double area = L * W;
        double perimeter = 2*L + 2*W;
        double volume = L * W * H;
        System.out.format("The area is roughly %.2fsqft%n", area);
        System.out.format("The perimeter is roughly %.2fsqft%n", perimeter);
        System.out.format("The volume is roughly %.2fsqft%n", volume);

    }
}
