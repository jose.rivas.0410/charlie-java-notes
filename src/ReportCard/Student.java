package ReportCard;

import java.util.ArrayList;
import java.util.List;

public class Student {
    private String name;
    private List<Integer> grades;

    public Student(String name) {
        this.name = name;
        this.grades = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void addGrade(int grade) {
        this.grades.add(grade);
//        grade.add(grades);
    }

    public double getGradeAverage() {
        int average = 0;
        for (int grade : this.grades) {
            average += grade;
        }
        return (double) average / this.grades.size();
    }

    public static void main(String[] args) {
        Student student = new Student("John");
        System.out.println(student.getName());
        student.addGrade(98);
        student.addGrade(100);
        System.out.println(student.getGradeAverage());

    }
}
