package Objects;

public class Person {
    // this has both static static and instance properties
    public static long worldPop = 7500000000L; // class property

    public String name; // instance property

    // psvm
    public static void main(String[] args) {
        Person theGreatestGuitaristEver= new Person();
        theGreatestGuitaristEver.name = "Eddie Van Halen";

        // access our static property
        Person.worldPop++;

        System.out.println(worldPop);

        System.out.println(theGreatestGuitaristEver.name);
//        System.out.println(Person.name);
    }

}
