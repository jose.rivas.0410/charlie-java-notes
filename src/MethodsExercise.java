import java.util.Scanner;

public class MethodsExercise {

    public static void main(String[] args) {

//        System.out.println(addition(20, 10));
//        System.out.println(subtraction(40, 30));
//        multiplication(20, 30);
//        division(20, 4);
//        moduloDivision(20, 3);


//        System.out.println(getRange("hello"));
//        getInteger(1, 10);


//        for (int counter = 1; counter <= 10; counter++) {
//            System.out.printf("%d! = %d\n", counter, factorial(counter));
//        }
        // Stephen's code
//        System.out.println(factorial(5));
        // test scannerFactorial()
//        Scanner sc = new Scanner(System.in);
//        scannerFactorial(sc);


//        average();


//        isLeapYear();
        // Stephen's code
//        Scanner sc = new Scanner(System.in);
//        isLeapYear2(sc);

    } // end of main method

//    public static int addition(int num1, int num2){
//        return num1 + num2;
//    }
//
//    public static int subtraction(int num1, int num2){
//        return num1 - num2;
//    }
//
//    public static void multiplication(int num1, int num2){
//        int times = num1 * num2;
//        System.out.format("Multiplication: %s\n", times);
//    }
//
//    public static void division(int num1, int num2){
//        return num1 / num2;
//    }

//    static void moduloDivision(int num1, int num2){
//        return num1 % num2;
//    }




//          My Code
//    private static int getRange(String quantity) {
//        int count = 0;
//        String input;
//        int range;
//            do {
//                input = JOptionPane.showInputDialog(quantity);
//                try {
//                    range = Integer.parseInt(input);
//                } catch(NumberFormatException e) {
//                    JOptionPane.showMessageDialog(null, "Sorry that input is not valid, please choose a quantity from 1-10");
//                    count++;
//                    range = -1;
//                }
//            } while((range > 10 || range < 1) && (count < 2));
//
//        if (count == 2) {
//            JOptionPane.showMessageDialog(null,
//                    "Sorry you failed to input a valid response, terminating.");
//            System.exit(0);
//        }
//        return range;
//    }

//    Stephen's Code
//    public static int getInteger(int min, int max) {
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Enter a valid number");
//        int userInput = sc.nextInt();
//        if (userInput >= min && userInput <= max) {
//            System.out.println("You entered " + userInput);
//            return userInput;
//        }
//        else {
//            System.out.println("Invalid entry: Number must be between " + min + " and " + max);
//        }
//        return getInteger(min, max);
//    }




//    My Code
//    public static long factorial(long number) {
//        if (number <= 1) return 1;
//        else return number * factorial(number - 1);
//    }

//    Stephen's Code
//    public static long factorial(int num) {
//        int output = 1;
//        for (int i = 1; i <= num ; i++) {
//            output *= i;
//        }
//        return output;
//    }
    // update factorial to be user interactive
//    public static void scannerFactorial(Scanner sc) {
//        boolean userContinue;
//        String userInput;
//
//        do {
//            System.out.println("Enter an integer from 1 to 10");
//            int userNumber = getInteger(1, 10);
//            System.out.println(factorial(userNumber));
//            do {
//                System.out.println("Would you like to continue? [y/n]");
//                userInput = sc.next();
//            } while (!userInput.equalsIgnoreCase("y") && !userInput.equalsIgnoreCase("n"));
//            userContinue = userInput.equalsIgnoreCase("y");
//        } while (userContinue);
//    }




//    public static void average() {
//        int numlenngth = 3;
//        double total = 0;
//        Scanner sc = new Scanner(System.in);
//
//        for (int num1 = 0; num1 < numlenngth; num1++) {
//            System.out.print("Please enter an Input: ");
//            total += sc.nextInt();
//        }
//        System.out.println("Average : " + (total / numlenngth));
//    }




    public static void isLeapYear() {
        String choice = "y";
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a year and see if it's a Leap Year:");
        long year = sc.nextLong();
        while (choice.equalsIgnoreCase("y")) {
            if (year != 0) {
                if (year % 400 == 0)
                    System.out.println(year + " is a leap year");
                else if (year % 100 == 0)
                    System.out.println(year + " is not a leap year");
                else if (year % 4 == 0)
                    System.out.println(year + " is a leap year");
                else
                    System.out.println(year + " is not a leap year");
            } else
                System.out.println("Year zero does not exist ");

            System.out.print("Continue? (y/n): ");
            choice = sc.next();
            System.out.println();
        }
    }

//    Stephen's Code
    public static boolean isLeapYear2(Scanner sc) {
        int year = sc.nextInt();
        if (year <= 0 || year > 9999) {
            System.out.println("Invalid year");
            return false;
        }
        else if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
            System.out.println("Leap Year!");
            return true;
        }
        else {
            System.out.println("Not a Leap Year");
            return false;
        }
    }

} // end of class
