package InheritanceGym;

public class GymManagement {
    public static void main(String[] args) {
//        NormalMember nm1 = new NormalMember("Jason", 122, 2012);
//
//        VIPMember vp1 = new VIPMember("John", 143, 2012);
//
//        nm1.calculateAnnualFee();
//        vp1.calculateAnnualFee();
//
//        nm1.displayMemberInfo();
//        vp1.displayMemberInfo();

        Member[] member = new Member[7];
        member[0] = new NormalMember("Joey",232, 2015);
        member[1] = new NormalMember("Shelly", 218, 2015);
        member[2] = new NormalMember("Vanessa", 101, 2012);
        member[3] = new VIPMember("Joe", 270, 2016);
        member[4] = new VIPMember("Jane", 338, 2020);
        member[5] = new NormalMember("Branden", 357, 2020);
        member[6] = new VIPMember("Nancy", 311, 2019);
        for (Member m : member) {
            m.displayMemberInfo();
            m.calculateAnnualFee();
        }
    }
}
