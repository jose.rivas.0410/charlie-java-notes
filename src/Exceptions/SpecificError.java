package Exceptions;

import Validation.Input;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SpecificError {
    public static void main(String[] args) {
        int choice = 0;
        Scanner input = new Scanner(System.in);
        int[] numbers = { 10, 11, 12, 13, 14, 15 };
//        System.out.print("Please enter the index of the array: ");

//        try {
//            choice = input.nextInt();
//            System.out.printf("numbers[%d] = %d%n", choice, numbers[choice]);
//        }catch (ArrayIndexOutOfBoundsException e1) {
//            System.out.println("Error: Index is invalid");
//        }catch (InputMismatchException e2) {
//            System.out.println("Error: You did not enter an integer.");
//        }catch (Exception e3) {
//            System.out.println(e3.getMessage());
//        }

        SpecificError sp = new SpecificError();
        System.out.println("Please enter a number: ");
//        int userInput = sp.getBinary();
        int userInput = sp.getHex();
        System.out.println(userInput);
    }
    Input in = new Input(new Scanner(System.in));

    public int getBinary() {
        int output;
        try {
            output = Integer.valueOf(in.getString(), 2);
        }catch (NumberFormatException e) {
            System.out.println("Invalid Binary Number");
            return getBinary();
        }
        return output;
    }

    public int getHex() {
        int output;
        try {
            output = Integer.valueOf(in.getString(), 16);
        }catch (NumberFormatException e) {
            System.out.println("Invalid Hex Number");
            return getHex();
        }
        return output;
    }
}
