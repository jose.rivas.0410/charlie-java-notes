package AbstractAndInterface;
/*
ABSTRACT CLASS - a special type of class that is strictly to be a 'base class'
for other classes to derive from
- cannot be instantiated
- may have fields and methods just like classes
- they have ABSTRACT METHODS
    - ABSTRACT METHODS - methods that has no body and MUST be implemented in the derived class
    - only exist in abstract class
    - doesn't contain a body

    how to make an abstract class?
    How to make a class, an abstract class?
    - use the 'abstract' keyword INSIDE OF THE CLASS DECLARATION
 */

public abstract class Wrestler {

    // METHOD
    public static void paymentForPerformance(int hours) {
        System.out.println("The wrestler's pay for tonight's performance: " + 250*hours);
    }


    // ABSTRACT METHOD
    abstract public void wrestlerName();

    public abstract void themeMusic();

    public abstract void finisher();



}
