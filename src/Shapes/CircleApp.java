package Shapes;

import java.util.Scanner;

public class CircleApp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Circle cle = new Circle(sc);
        System.out.println("Enter a radius: ");
        System.out.println(cle.getRadius());
        System.out.format("The radius you entered was %scm\n", cle.getRadius());
        System.out.format("Area is %.3fcm %n", cle.getArea());
        System.out.format("Circumference is %.3fcm %n", cle.getCircumference());
    }
}
