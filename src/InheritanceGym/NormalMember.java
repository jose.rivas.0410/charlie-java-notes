package InheritanceGym;

public class NormalMember extends Member{
    public NormalMember() {
        System.out.println("Child Constructor with no parameters");
    }

    public NormalMember(String name, int memberID, int memberSince) {
        super(name, memberID, memberSince);
        System.out.println("Child Constructor with 3 parameters");
    }

    @Override
    public void calculateAnnualFee() {
//        annualFee = (1-0.01*discount)*(100 + 12*30);
        annualFee = (1-0.01*getDiscount())*(100 + 12*30);
    }
}
