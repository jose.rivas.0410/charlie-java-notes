import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ServerNameGenerator {
    public static void main(String[] args) {
        String[] adj = {"Determined", "Average", "Muddy", "Curious", "Unusual", "Fine", "Talented", "Energetic", "Defiant", "Adorable"};
        String[] noun = {"Child", "continent", "patriotism", "man", "teenager", "woman", "book", "computer", "pride", "alligator"};

        List<String[]> arrayList = new ArrayList<>();
        arrayList.add(adj);
        arrayList.add(noun);
        Random random = new Random();
        System.out.println("Your server name is: ");
        for(String[] currentArray : arrayList){
            String chosenString = currentArray[random.nextInt(currentArray.length)];
            System.out.print(chosenString + " ");
        }
    }
}
