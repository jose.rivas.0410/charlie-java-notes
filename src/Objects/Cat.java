package Objects;

public class Cat {
//    instance variables: state(s) of the Cat class
    String name;
    int age;
    String color;
    String breed;

//    instance methods: "behaviors" of the Cat
    public static void sleep() {
        System.out.println("This cat is now sleeping...");
    }

    public static void play() {
        System.out.println("This cat is now playing...");
    }

    public static void feed() {
        System.out.println("This cat is now eating...");
    }


    public String greeting() {
        return String.format("Hello this is %s", name);
    }
}
