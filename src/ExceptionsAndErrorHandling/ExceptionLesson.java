package ExceptionsAndErrorHandling;
/*
EXCEPTION HANDLING / ERROR HANDLING
-allows
 */
public class ExceptionLesson {
    public static void main(String[] args) {

        // EXAMPLE try... catch block
//        try {
//            int divideByZero = 5 / 0;
//            System.out.println("Rest of code in try block");
//        }catch (ArithmeticException e) {
//            System.out.println("ArithmeticException: " + e.getMessage());
//             display the default built-in message for ArithmeticException
//        }

//       Example Multiple Catch Block
//        class ListOfNumbers {
//            public int[] arrayOfNumbers = new int[10];
//
//            public void writeList() {
//                try {
//                    arrayOfNumbers[10] = 11;
//                }catch (NumberFormatException e1) {
//                    System.out.println("NumberFormatException: " + e1.getMessage());
//                }catch (IndexOutOfBoundsException e2) {
//                    System.out.println("IndexOutOfBoundsException: " + e2.getMessage());
//                }
//            }
//        } // end of ListOfNumbers class

//        ListOfNumbers list = new ListOfNumbers();
//        list.writeList();

        try {
            int divideByZero = 5 / 10;
//            System.out.println("Rest of code in try block");
        }catch (ArithmeticException e) {
            System.out.println("ArithmeticException: " + e.getMessage());
        }finally {
            System.out.println("nvjlnwnjwrnnv");
        }

    }
}
