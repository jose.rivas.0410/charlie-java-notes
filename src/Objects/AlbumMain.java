package Objects;

public class AlbumMain {
    public static void main(String[] args) {
        Album album1 = new Album();
        Album album2 = new Album();

        // whenever we use the 'new' keyword,
        // we are "creating" / instantiating an object of
        // a class
    }
}
