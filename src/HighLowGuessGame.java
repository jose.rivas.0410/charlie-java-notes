import java.util.Random;
import java.util.Scanner;

public class HighLowGuessGame {

    public static void main(String[] args) {

        guess();
    }

    public static void guess() {

        Scanner sc = new Scanner(System.in);

        Random rd = new Random();

        int tries = 0;
        String choice = "y";
        int correctNum = rd.nextInt(100);

        while(choice.equalsIgnoreCase("y")) {
            while (true) {
                System.out.println("Enter a number 0-100: ");
                int guess1 = sc.nextInt();
                double guess = Integer.parseInt(String.valueOf(guess1));
                if (guess < correctNum) {
                    System.out.println("HIGHER!");
                    tries++;
                }
                else if (guess > correctNum) {
                    System.out.println("LOWER!");
                    tries++;
                }
                else if (guess == correctNum) {
                    System.out.println("CORRECT! Your number was " + correctNum);
                    if (tries > 1) {
                        System.out.println("It only took you " + tries + " attempts!");
                    } else {
                        System.out.println("You guessed it first try! good job");
                    }
                    break;
                }
                else {
                    System.out.println("not a valid option");
                }
            }
            System.out.print("Continue? (y/n): ");
            choice = sc.next();
            System.out.println();
        }
    }
}
