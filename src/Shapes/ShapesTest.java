package Shapes;

public class ShapesTest {
    public static void main(String[] args) {
//        Rectangle rec_box = new Rectangle(4, 5);
//        Square rec_box2 = new Square(6);
        Measurable myShape;
        myShape = new Rectangle(5, 2);

//        System.out.println(rec_box.getArea());
//        System.out.println(rec_box.getPerimeter());
//
//        System.out.println(rec_box2.getArea());
//        System.out.println(rec_box2.getPerimeter());

        System.out.println(myShape.getArea());
        System.out.println(myShape.getPerimeter());

        myShape = new Square(2);

        System.out.println(myShape.getArea());
        System.out.println(myShape.getPerimeter());
    }
}
