import java.util.Arrays;

public class StringMethodsExercise {
    public static void main(String[] args) {

//        String myLength = "Good afternoon, good evening, and good night";
//        System.out.println("String length is: " + myLength.length());

//        String uCase = "Good afternoon, good evening, and good night";
//        System.out.println(uCase.toUpperCase());

//        String uCase = "Good afternoon, good evening, and good night";
//        System.out.println(uCase.toLowerCase());

//        String firstSubstring = "Hello World".substring(6);
//        System.out.println(firstSubstring);
//        String firstSubstring = "Hello World".substring(3);
//        System.out.println(firstSubstring);
//        String firstSubstring = "Hello World".substring(10);
//        System.out.println(firstSubstring);

//        String message = "Good evening, how are you?".substring(0, 12);
//        System.out.println(message);
//        String message = "Good evening, how are you?".substring(14, 26);
//        System.out.println(message);

//        char myChar = "San Antonio".charAt(0);
//        System.out.println(myChar);
//        char myChar = "San Antonio".charAt(4);
//        System.out.println(myChar);
//        char myChar = "San Antonio".charAt(7);
//        System.out.println(myChar);

//        String alpha = "Jose Josh Nick Stephen Karen";
//        String[] splitAlpha = alpha.split(" ");
//        System.out.println(Arrays.toString(splitAlpha));

//        String m1 = "Hello, ";
//        String m2 = "how are you?";
//        String m3 = "I love Java!";
//        System.out.format("%s%s %s",m1, m2, m3);

//        int result = 89;
//        System.out.format("You scored %s marks for your test!", result);

    }
}
