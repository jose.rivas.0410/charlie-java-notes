package ContactsBonusExercise;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int input;
        String line;
        BufferedReader br = null;
        System.out.println("╔═════════════════╗");
        System.out.println("╠    MAIN MENU    ╣");
        System.out.println("╚═════════════════╝");
        System.out.println("1. View Contacts\n" +
                "2. Add a New Contact\n" +
                "3. Search a contact by name\n" +
                "4. Delete an existing contact\n" +
                "5. Exit\n" +
                "Enter an option (1, 2, 3, 4 or 5):");
        input = sc.nextInt();
        switch (input) {
            case 1:
                try {
                    br = new BufferedReader(new FileReader("src/ContactsBonusExercise/contacts/contactlist.txt"));
                    line = br.readLine();
                    while (line != null) {
                        System.out.println(line);
                        line = br.readLine();
                    }
                }catch (IOException e) {
                    System.out.println(e.getMessage());
                }finally {
                    try {
                        if (br != null) {
                            br.close();
                        }
                    }catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                }
                break;
            case 2:
                String name = sc.nextLine();
                String num = sc.nextLine();
                try (BufferedWriter w = new BufferedWriter(new FileWriter("src/ContactsBonusExercise/contacts/contactlist.txt"))) {
                    w.write(name);
                    w.write(num);
                    w.newLine();
                }catch (IOException e) {
                    System.out.println(e.getMessage());
                }
                break;
            case 3:
                String userInput;
                System.out.println("Enter the name of the contact: ");
                userInput = sc.nextLine();
                try (BufferedWriter w = new BufferedWriter(new FileWriter("src/ContactsBonusExercise/contacts/contactlist.txt"))) {
                    w.equals("Jason");
                    w.equals("Alex");
                    w.equals("Jeff");
                    w.equals("Brenda");
                    w.equals("Alexis");
                    w.equals("Tom");
                    w.equals("Erika");
                    System.out.println(w);
                }catch (IOException e) {
                    System.out.println(e.getMessage());
                }
                break;
            case 4:
                File contact = new File("src/ContactsBonusExercise/contacts/contactlist.txt");
        }
    }
}
