package CodeboundGymProject;

public class SingleClubMember extends Member {
    private int club;

    public SingleClubMember(String memberType, int memberID, String name, double fees, int club) {
        super(memberType, memberID, name, fees);
        this.club = club;
    }

    public int getClub() {
        return club;
    }

    public void setClub(int club) {
        this.club = club;
    }

    @Override
    public String toString() {
        return this.getMemberType() + "," + this.getMemberID() + "," + this.getName() + "," + this.getFees() + "," + club;
    }


}
