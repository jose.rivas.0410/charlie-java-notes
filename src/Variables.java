public class Variables {
    public static void main(String[] args) {

//        int favoriteNum = 7;
//        System.out.println(favoriteNum);
//        String myName = "Jose";
//        System.out.println(myName);

//        char myName = 'J';
        // prints out the single character J
//        char myName = 3.14159;
        // wants to change char to double

//        long myNum;
//        System.out.println(myNum);
        // wants to initialize myNum
//        long myNum = 3.14;
        // wants to change long to double

//        long myNum = 123L;
//        System.out.println(myNum);
        // the 'L' is not shown in the console
//        long myNum = 123;
//        System.out.println(myNum);
        // value appears from when initializing it to a number

//        float myNum = (float) 3.14;
//        float myNum = 3.14f;
        // wants to refactor the code to float nyNum = (float) 3.14;

//        int x = 10;
//        System.out.println(x++); // 10
//        System.out.println(x); // 11
        // shows in console a 10 then 11 to signify it increasing by one

//        int x = 10;
//        System.out.println(++x); // 11
//        System.out.println(x); // 11
        // shows both outputs as 11 to let user know that it increased
        // value by one, then displayed it in the console

//        int class;
        // calling a variable 'class' does not work since class is
        // a data type

//        String theNumberEight = "eight";
//        Object o = theNumberEight;
//        int eight = (int) o;
//        System.out.println(eight);
        // does not work and gives an error of not being able to cast
        // String to int

//        int eight = (int) "eight";
        // gives error for not being able to cast String to int

//        int x = 5;
//        x += 6;
//        System.out.println(x);
//        int a = 7;
//        int b = 8;
//        b *= a;
//        System.out.println(b);
//        int c = 20;
//        int d = 2;
//        c /= d;
//        d -= c;
//        System.out.println(d);

        int z = Integer.MAX_VALUE + 1;
        System.out.println(z);

    }
}
