package Inheritance;

public class Main {
    public static void main(String[] args) {
//        CREATE A NEW ANIMAL OBJECT
        Animal x = new Animal("Animal", 1, 1, 5, 10);

//        create a new Dog object
        Dog dog1 = new Dog("Chula", 1, 1, 5, 20, 2, 4, 1, 25, "Long Silky Fur");

        // call a method from the Dog class
        dog1.eat();

        x.eat();
    }
}
