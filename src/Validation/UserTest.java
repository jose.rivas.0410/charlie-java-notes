package Validation;

import java.util.Scanner;

public class UserTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Input input = new Input(sc);

        System.out.println("Enter a word: ");
        System.out.println(input.getString());
        System.out.println("Enter a number between 1-100: ");
        System.out.println(input.getInt(1, 100));
        System.out.println("Enter a decimal: ");
        System.out.println(input.getDouble());
        System.out.println("Enter a decimal between 0.0-30.0: ");
        System.out.println(input.getDouble(0.0, 30.0));
//        System.out.println(input.yesNo());

    }
}
