package FileHandling;
/*
Main Focus: File, BufferedReader, FileReader, BufferedWriter,
and FileWriter from java.io package.

how to import?
import java.io.*











Alternate java libraries
Path Interface, Paths class,
Files class from the java.io and java.nio packages

File Class
- represents the files and directory pathNames in an abstract manner.
- used for creating files and directories, file searching and
file deletion

BufferedReader Class
- reads text from a character-input steam.
- buffered characters so as to provide for the efficient
reading of characters, arrays, and lines


BufferedWriter Class
- creates a buffered character-output stream that uses a default-sized
output buffer- it inherits the Writer Class


FileWriter Class
- used to write character-oriented data to a file
-you don't need to convert string into byte array
    - provides method to write string(s) directly

FileReader Class
- used to read data from the file
-return data in byte format

Paths:
- absolute paths: specified from the filestream root
- relative paths: relative to the CURRENT WORKING DIRECTORY


EX
/
    Users/
        codebound/
            Documents/
                important-stuff.txt
            IdeaProjects/
                charlie-java-notes/  <-- current working directory
                    src/
                        FileTastic.java
                    data/
                        data.txt

relative paths
- data/data.txt
-.data/data.txt

absolute paths
- /Users/codebound/IdeaProjects/charlie-java-notes/data/data.txt

 */

import java.io.*;

public class FileHandlingLesson {
    public static void main(String[] args) {

        // READING IF THERE'S A TEXT WITH A FILE NAME...
        // use the FileReader class w/ a BufferedReader class

        // create a string variable
//        String line;

        // create a BufferedReader object
//        BufferedReader bufferedReader = null;

        // try-catch-finally statement
//        try {
//            bufferedReader = new BufferedReader(new FileReader("src/FileHandling/movieQuotes/hamlet.txt"));
//
//            line = bufferedReader.readLine();
//
//            while (line != null) {
//                System.out.println(line);
//                line = bufferedReader.readLine();
//            }
//        }catch (IOException e) {
//            System.out.println(e.getMessage());
//        }finally {
//            try {
//                if (bufferedReader != null) {
//                    bufferedReader.close();
//                }
//            }catch (IOException e) {
//                System.out.println(e.getMessage());
//            }
//        }






        // WRITING TO A TEXT FILE...
        // use a FileWriter class w/ a BufferedWriter class

        // create a string variable
        String name = "Ronald Reagan\n";
        String presNum = "40\n";
        String party = "Republican\n";
        String yrsServed = "8\n";
        String famQuote = "Mr. Gorbachev, open this gate. Mr. Gorbachev, tear down this wall!\n";

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/student66/IdeaProjects/charlie-java-notes/data/Ronald-Reagan.txt"))) {
            writer.write(name);
            writer.write(presNum);
            writer.write(party);
            writer.write(yrsServed);
            writer.write(famQuote);
            writer.newLine();
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }


        // OVERWRITING A TEXT FILE / FILE'S TEXT...

        // create a string variable
//        String overwriteText = "Listen to many, speak to a few";
//        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("src/FileHandling/movieQuotes/hamlet.txt"))) {
//            rewrite.write(overwriteText);
//            rewrite.newLine();
//        }catch (IOException e) {
//            System.out.println(e.getMessage());
//        }

        // RENAMING A TEXT FILE
//        File olfFileName = new File("src/FileHandling/movieQuotes/terminator.txt");
        // targeting the file we want to rename

//        File newFileNAme = new File("src/FileHandling/movieQuotes/terminator2.txt");
        // creating the name that we want

//        olfFileName.renameTo(newFileNAme);

        // DELETING A TEXT FILE...
//        File newFileName = new File("src/FileHandling/movieQuotes/terminator2.txt");
//
//        newFileName.delete();
        // cli: 'rm-rf fileName'

    }
}
