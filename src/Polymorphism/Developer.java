package Polymorphism;

public class Developer {

    public String work() {
        return "I'm coding from the office";
    }

    // final keyword
//    public final String work() {
//        return "I'm NOT coding, but I'm at home...haha";
//    }
    // - prevents the ScrumMaster class from INHERITING the work() method
}
