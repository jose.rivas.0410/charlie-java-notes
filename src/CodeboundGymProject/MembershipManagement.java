package CodeboundGymProject;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

public class MembershipManagement {
    private final Scanner userInput = new Scanner(System.in);

    private int getIntInput() {
//        int input;
//        input = userInput.nextInt();
//        return input;
        int choice = 0;
        while (choice == 0) {
            try {
                choice = userInput.nextInt();
                if (choice == 0)
                    throw new InputMismatchException();
                userInput.nextLine();
            }
            catch (InputMismatchException e) {
                userInput.nextLine();
                System.out.print("\nERROR: INVALID INPUT. Please try again: ");
            }
        }
        return choice;
    }

    private void printClubOptions() {
        System.out.println("1.) Club Alpha\n2.) Club Bravo\n3.) Club Charlie\n4.) Multi Clubs");
    }

    public int getChoice() {
        int choice;
        System.out.println("\n" +
                " __    __     _                            _            ___        ___           ___                 \n" +
                "/ / /\\ \\ \\___| | ___ ___  _ __ ___   ___  | |_ ___     / __\\___   / __\\ ___     / _ \\_   _ _ __ ___  \n" +
                "\\ \\/  \\/ / _ | |/ __/ _ \\| '_ ` _ \\ / _ \\ | __/ _ \\   / /  / _ \\ /__\\/// _ \\   / /_\\| | | | '_ ` _ \\ \n" +
                " \\  /\\  |  __| | (_| (_) | | | | | |  __/ | || (_) | / /__| (_) / \\/  | (_) | / /_\\\\| |_| | | | | | |\n" +
                "  \\/  \\/ \\___|_|\\___\\___/|_| |_| |_|\\___|  \\__\\___/  \\____/\\___/\\_____/\\___/  \\____/ \\__, |_| |_| |_|\n" +
                "                                                                                     |___/           ");
        System.out.println("═════════════════════════════════════════════════════════════════════════════════════════════════════\n" +
                "Please select an option\n" +
                "(or Enter -1 to quit):\n" +
                "1) Add Member\n" +
                "2) Remove Member\n" +
                "3) Display Member Information\n" +
                "═════════════════════════════");
        choice = getIntInput();
        return choice;
    }

    public String addMembers(LinkedList<Member> m) {
        String name;
        int club;
        String mem;
        double fees;
        int memberID;
        Member mbr;
        Calculator<Integer> cal;
        System.out.println("Please enter the member's name: ");
        name = userInput.nextLine();
        System.out.println("Please enter clubs ID: ");
        printClubOptions();
        club = userInput.nextInt();
        if (m.size() > 0) {
            memberID = m.getLast().getMemberID() + 1;
        }else {
            memberID = 1;
        }
        if (club > 0 && club <= 3) {
            cal = (s) -> {
                switch (s) {
                    case 1:
                        return 900;
                    case 2:
                        return 950;
                    case 3:
                        return 1000;
                    default:
                        return -1;
                }
            };
            fees = cal.calculateFees(club);
            mbr = new SingleClubMember("S", memberID, name, fees, club);
            m.add(mbr);
            mem = mbr.toString();
            System.out.println("STATUS: Single Club Member added");
        }else {
            cal = (m2) -> {
                switch (m2) {
                    case 4:
                        return 1200;
                    default:
                        return -1;
                }
            };
            fees = cal.calculateFees(club);
            mbr = new MultiClubMember("M", memberID, name, fees, club);
            m.add(mbr);
            mem = mbr.toString();
            System.out.println("STATUS: Multi Club Member added");
        }
        return mem;
    }

    public void removeMember(LinkedList<Member> m) {
        int memberID;
        System.out.println("Enter the member's ID that you want to remove: ");
        memberID = getIntInput();
        for ( int i = 0; i < m.size(); i++) {
            if (m.get(i).getMemberID() == memberID) {
                m.remove(i);
                System.out.println("Member removed!");
                return;
            }
        }
        System.out.println("Member ID not found");
    }

    public void printMemberInfo(LinkedList<Member> m) {
        int memberID;
        System.out.println("Enter Member's ID to display information: ");
        memberID = getIntInput();
        for (int i = 0; i < m.size(); i++) {
            if (m.get(i).getMemberID() == memberID)
            {
                String[] info = m.get(i).toString().split(",");

                System.out.format("Member Type: %s\nMember ID: %s\nMember Name: %s\nMembership Fees: $%s\nClub ID: %s\n",info[0], info[1], info[2], info[3], info[4]);
                return;
            }
        }
        System.out.println("Member ID not found");

    }

}
