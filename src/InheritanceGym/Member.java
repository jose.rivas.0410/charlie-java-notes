package InheritanceGym;

import java.util.Scanner;

public abstract class Member {
    public String welcome = "Welcome to Inherit Fitness!";
    protected double annualFee;
    private String name;
    private int memberID;
    private int memberSince;
    private int discount;

    public Member() {
        System.out.println("Parent Constructor with no parameters");
    }

    public Member(String name, int memberID, int memberSince) {
        this.name = name;
        this.memberID = memberID;
        this.memberSince = memberSince;
        System.out.println("Parent Constructor with 3 parameters");
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
        Scanner sc = new Scanner(System.in);
        String adminPassword;
        System.out.println("Please enter the admin password: ");
        adminPassword = sc.nextLine();
        if (!adminPassword.equals("abcd")) {
            System.out.println("User entered an invalid password");

        }else {
            System.out.println("Please enter the discount: ");
            discount = sc.nextInt();
            System.out.println("Discount is " + discount);
            System.out.println("Discount applied " + discount);
        }
    }

    public void displayMemberInfo() {
        System.out.println(name);
        System.out.println(memberID);
        System.out.println(memberSince);
        System.out.println(annualFee);
    }

//    public void calculateAnnualFee() {
//        annualFee = 0;
//    }

    public abstract void calculateAnnualFee();

}
