package AbstractInterfaceDemo;

public class Main {
    public static void main(String[] args) {
        MyClass mc = new MyClass();
        mc.someMethod();
        System.out.println(mc.myInt);
        MyInterface.someStaticMethod();
        mc.someDefaultMethod();
    }
}
