public class MethodLesson {
    public static void main(String[] args) {
        // call our greeting()
//        System.out.println(greeting("Charlie"));

        // calling our sayHi() and passing in values for the parameters
//        sayHi("Hey there", "Josh");

        // call or returnFive()
//        System.out.println();
//        System.out.println(returnFive());

//        System.out.println(yelling("I'm not yelling"));

//        sayHello(3); // World Hello
                          // World Hello
                          // World Hello
//        sayHello("Charlie"); // Charlie Hello
//        sayHello("Jose", "Hey"); // Hey Jose



//        String changeMe = "Hello CodeBound";
        // call our changeThis()
//        changeThis(changeMe);

//        System.out.println(changeMe);

        // Recursion
        int result = addNums(10);
        System.out.println(result);


    } // end of main method

//    Syntax of defining a method
//    public static returnType methodName(para2, para2, para3,...) {
//        the statement(s) of the method
    //    a return statement
//    }

//    public static String greeting(String name) {
//        return String.format("Hello there, %s", name);
//    }
    // public - this is the VISIBILITY MODIFIER
    // static - the presence of this keyword defines the method belongs
    // to the class, as opposed to instances of it.

    // String - the return type of the method
    // greeting - name of the method


//    public static void sayHi(String greeting, String name) {
//        System.out.printf("%s %s!\n", greeting, name);
//    }


//    public static int returnFive() {
//        return 5;
//    }


//    public static String yelling(String s) {
//        return s.toUpperCase() + "!";
//    }









//    METHOD OVERLOADING
    // - defining multiple methods with the same name, but with different
    // parameter types, parameter order, or number of parameters

    // v1
//    public static void sayHello(int num) {
//        for (int i = 0; i < num; i++) {
//            sayHello();
//        }
//    }

    // v2
//    public static void sayHello() {
//        sayHello("Hello", "World");
//    }

    // v3
//    public static void sayHello(String name) {
//        sayHello("Hello", name);
//    }

    // v4
//    public static void sayHello(String name, String greeting) {
//        System.out.println(greeting + " " + name);
//    }

//    PASSING PARAMETERS TO METHODS
//    public static void changeThis(String sentence) {
//        sentence = "Denim Denim Denim";
//    }






    // Recursion
    // The technique of making a method call itself
    // - provides a way to break complicated problems which are easier to solve

    // use recursion to add all of the numbers up to 10
    public static int addNums(int num) {
        if (num > 0) {
            return num + addNums(num - 1);
        }
        else {
            return 0;
        }
    }




} // end of class
