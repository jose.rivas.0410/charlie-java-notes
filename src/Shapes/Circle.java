package Shapes;

import java.util.Scanner;

public class Circle {
    private Scanner sc;
    private double radius;
    private final double PI = 3.14159;

    public Circle(Scanner sc) {
        radius = sc.nextDouble();
    }

    public Circle(double r) {
        radius = r;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return PI * radius * radius;
    }

    public double getCircumference() {
        return 2 * PI * radius;
    }

}
